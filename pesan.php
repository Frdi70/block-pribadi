<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST["Name"];
    $email = $_POST["email"];
    $message = $_POST["Message"];

    $to = "ferdiyansah.mukti@gmail.com";
    $subject = "Pesan dari " . $name;
    $headers = "From: " . $email . "\r\n";
    $headers .= "Reply-To: " . $email . "\r\n";

    if (mail($to, $subject, $message, $headers)) {
        echo "<p>Pesan berhasil terkirim. Terima kasih telah menghubungi kami!</p>";
    } else {
        echo "<p>Maaf, terjadi kesalahan saat mengirim pesan.</p>";
    }
}
?>
